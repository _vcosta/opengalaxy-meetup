# O que é?

One-Stop-Shop para Big Data Analytics e IA

# Mandala

## Data Lake
Solução completa para se implementar um modelo data centric.

Workflow
    - Pipelines de ingestão
        - api
        - bancos
        - arquivos

Discovery
    - Exploração dos dados, Visualizações, etc.

Data Intelligence
    - Data science para predição de cenários

## Search

Enterprise search
    - Buscar qualquer tipo de dadods em apps/sites

Observability
    - Monitoramento de infra e aplicação centralizado

Security
    - Detectar anomalias nos comportamentos de sua infra
        - falhas

## DOC (Data Operation Center)

Suporte 24/7 a todos os clientes
    - Infra por conta da semantix

# Produto

## Hoje

Hoje oferecemos ambientes cloud com ecossistema Hadoop e algumas outras ferramentas que nasceram no contexto on-premise.

## Amanhã

Estamos atualizando nossa plataforma pra um conceito Cloud Native e utilizando Kubernetes pra fazer toda a orquestração dos serviços
além estarmos modularizando mais a solução, para ser algo mais customizável a necessidade dos clientes.

e junto a isso, estamos trazendo outros produtos da semantix pra dentro do opengalaxy. tanto para validação interna do produto quanto para disponibilizar esses produtos como módulos da plataforma

# Roadmap

Aqui da pra ver um pouco melhor a questão da modularização.
Hoje estamos (Time OpenGalaxy) trabalhando na parte "On demand" e "Storage", parte central do diagrama.

E os outros produtos Semantix é que formarão a parte mais externa do diagrama.

# Processo

## Hoje

Estamos a uns 6 meses já reestruturando tudo que já tinhamos.
    - Criamos toda nossa infraestrutura como código
Utilizamos Kubernetes para uma coisa ou outra.
Temos muita automação com Jenkins e vários scripts Python e Shell.

## Amanhã

Estamos estudando como automatizar nossa infra
    - Pipelines e Testes
Utilizando Kubernetes para todas as soluções novas que estamos implementando
Automação com ferramentas que já nascem no contexto de containers
    - Argo
    - Gitlab CI
Nossos scripts de infra estão sendo migrados pra Golang
    - Binário
    - Dependências
    - Implementação nos clientes
E também estamos reestruturando nosso backend para microsserviços em Elixir
    - Segregação de domínios
    - menor acoplamento
    - ferramentas mais adequadas ao nosso time
        - documentação
        - testes

# Cultura

## Antes

Tinhamos um time bem dividido e as responsabilidades eram jogadas de um lado pra outro e não tinhamos padrão de desenvolvimento.
    - Responsáveis por tal código
    - Conhecimento centralizado em uma pessoa
        - Problemas quando a pessoa não estava disponível
    - achar "culpados"
    - Difícil passagem de conhecimento

## Depois

Hoje estamos muito mais alinhados a cultura DevOps, nos vemos como um Time Opengalaxy.
Estamos distribuindo as tasks mais pelo interesse dos desenvolvedores do que pelo conhecimento prévio que eles já tem.
    - Pessoas mais engajadas nas tarefas
    - Mentoria constante
    - Mais pessoas conhecem o panorama geral do produto
    - Diversos pontos de vista
Melhorando nossos processos de desenvolvimento
    - Code Review
    - Automatização de tudo que é possível
        - Menos falha humana
        - Processos como código
Home-office
    - Ja faziamos home-office
    - mesmo assim a forma de trabalhar mudou muito
        - trabalhar na pandemia != trabalhar em casa

Considerações
    - O processo ficou um pouco mais demorado, mas o produto é hoje entregue com muito mais qualidade
    - Ficamos mais tempo no desenvolvimento e menos tempo resolvendo problemas depois da implementação

# Automação

Deixar o trabalho repetitivo pras máquinas e ter mais tempo para evoluirmos.
    - Melhorar a qualidade de vida/trabalho

# Equipe

- Dobramos o numero de desenvolvedores nos ultimos 5 meses
    - Buscando melhorias no nosso processo de on-boarding
- Multidisciplinaridade da equipe (Backend/Infra)
    - SysAdmin
    - Eng. Software
    - Cryptografia / Segurança
    - Eng. Dados

# Desafios

- Muita tecnologia nova
    - Integrar com a plataforma nova
    - Integração com o que ja temos
    - Envolve muita pesquisa, estudo e testes
- Time muito Dinâmico
    - Time core (fixo)
    - Pessoas de outros times vão sendo anexadas temporariamente (Integração)
    - sofrendo mudanças muito rápidas
- Experimentação
    - Decisões tomadas em conjunto
        - Não são feitas top-down
    - Estamos constantemente experimentando novas tecnologias, novos processos e adequando aquilo que funciona melhor pra nós.
- Quarentena
    - Possibilitou a implementação de muitas coisas
    - Forçou outras
        - estamos nos adequando