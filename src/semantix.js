const blue = '#0af'


export default {
  googleFont: 'https://fonts.googleapis.com/css2?family=Poppins',
  colors: {
    text: '#383838',
    background: '#fff',
    primary: blue,
    black: '#000',
  },
  fontWeights: {
    heading: 600,
    bold: 600,
  },
  text: {
    heading: {
      textTransform: 'uppercase',
      letterSpacing: '0.1em',
    },
  },
  styles: {
    pre: {
      color: 'primary',
      bg: '#000',
    },
    code: {
      color: 'primary',
    },
    Footer:{
      padding: '20px 80px',
      textAlign: 'right',
      // borderTop: '1px solid #383838',
      width: '100vw',
      display: 'flex',
      justifyContent: 'space-between'
    },
  }
}

